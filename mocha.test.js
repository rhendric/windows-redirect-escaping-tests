const assert = require('assert');
const et = require('./escapingTest');

it('escaping', () => assert.strictEqual(et.run(), '\\"<\n'));
