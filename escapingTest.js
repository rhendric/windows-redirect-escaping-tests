const { execSync } = require('child_process');

module.exports.run = () =>
    execSync(
        `"${process.execPath}" -p process.argv[1] -- ${
            process.platform === 'win32' ?
                '^"\\\\\\^"^<^"' :
                '\'\\"<\''}`,
        { encoding: 'utf8' });
