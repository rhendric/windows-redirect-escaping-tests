const test = require('ava');
const et = require('./escapingTest');

test('escaping', t => t.is(et.run(), '\\"<\n'));
